import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { FoodService } from '../food.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-update-food',
  templateUrl: './update-food.page.html',
  styleUrls: ['./update-food.page.scss'],
})
export class UpdateFoodPage implements OnInit {
  @Input() edit;

  foodName
  foodDate
  addedFood= {}

  constructor(public modalCtrl:ModalController, public foodService: FoodService, private alertCtrl: AlertController) { }

  ngOnInit() {

    this.foodName=this.edit.value.foodName
    this.foodDate=this.edit.value.foodDate

    // console.log(this.edit)
  }

  async dismis(){
    await this.modalCtrl.dismiss()
  }

  async update(){
    this.addedFood = ({itemFood:this.foodName,
                      itemDate:this.foodDate})
      let uid = this.edit.key
      await this.foodService.updateFood(uid, this.addedFood)      
      this.dismis()
  }

  async doneEdit() {
    const alert = await this.alertCtrl.create({
      header: 'Alert',
      subHeader: 'Update Food',
      message: 'Done updating food',
      buttons: ['OK']
    });

    await alert.present();
  }
}
