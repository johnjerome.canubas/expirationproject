import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage-angular';


@Injectable({
  providedIn: 'root'
})
export class FoodService {

  constructor(private storage: Storage) { 
    this.init()
  }

  addAFood(key, value){
      this.storage.set(key,value)
  }

  deleteFood(key){
    this.storage.remove(key);
  }

  updateFood(key, newValue){
    this.storage.set(key, newValue)
    this.getAllFood()
  }
  
  getAllFood(){
    let food: any = []
    this.storage.forEach((key, value, index) => {
    food.push({'key': value, 'value': key})
    });

    return food

  }

  async init(){
    await this.storage.create()
  }
}
