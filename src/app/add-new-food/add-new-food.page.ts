import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { FoodService } from '../food.service';
import { AlertController } from '@ionic/angular';


@Component({
  selector: 'app-add-new-food',
  templateUrl: './add-new-food.page.html',
  styleUrls: ['./add-new-food.page.scss'],
})
export class AddNewFoodPage implements OnInit {

  foodName
  foodDate
  addedFood= {}

  constructor(public modalCtrl:ModalController, public foodService:FoodService, private alertCtrl: AlertController) { }

  ngOnInit() {
  }

  async dismis(){
    await this.modalCtrl.dismiss(this.addedFood)
  }

  async addFood(){
    this.addedFood = ({itemFood:this.foodName,
                      itemDate:this.foodDate})
          console.log(this.addedFood);
          let uid = this.foodName + this.foodDate

    if(uid){
      await this.foodService.addAFood(uid, this.addedFood)
    }else{
      console.log("Can't Save an Empty Field")
    }
        this.dismis()
  }

  async presentAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Alert',
      subHeader: 'Add Food',
      message: 'Done adding food',
      buttons: ['OK']
    });

    await alert.present();

  }

}
