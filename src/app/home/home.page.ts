import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AddNewFoodPage } from '../add-new-food/add-new-food.page';
import { FoodService } from '../food.service';
import { UpdateFoodPage } from '../update-food/update-food.page';
import { Share } from '@capacitor/share';
import { AlertController } from '@ionic/angular';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {


  foodExpiry = []
  selectedFood = []
  selectedFoodName: string
  selectedFoodExpiry: string

  today : number = Date.now()

  constructor(public modalCtrl:ModalController, public foodService:FoodService, public alertController: AlertController) 
  
  {
    this.getAllFood()
  }

  doRefresh(event) {
    console.log('Begin async operation');

    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }
  
  ngOnInit() 
  {

  }

  async addFood(){
    const modal = await this.modalCtrl.create({
      component: AddNewFoodPage
    })

    modal.onDidDismiss().then(newAddedFood =>{
      this.getAllFood()


    })

      return await modal.present()
  }

  getAllFood(){
    this.foodExpiry = this.foodService.getAllFood()
    console.log(this.foodService.getAllFood());
  }
  deleteFood(key){
    this.foodService.deleteFood(key)
    this.getAllFood()
    // this.foodExpiry.splice(index,1)
  }

  async update(selectedFood){
    const modal = await this.modalCtrl.create({
      component: UpdateFoodPage,
      componentProps: {edit: selectedFood}
    })

    modal.onDidDismiss().then(()=>{
      this.getAllFood()
    })

    return await modal.present()
  }
async share(){

  await Share.share({
    title: 'Sharing this Ionic Application just for you!',
    text: 'GitLab Repository URL: ',
    url: 'https://gitlab.com/johnjerome.canubas/expirationproject',
  });
  
}

async foodDeleted() {
  const alert = await this.alertController.create({
    header: 'Alert',
    subHeader: 'Delete Food',
    message: 'Food Deleted Successfully',
    buttons: ['Proceed']
  });

  await alert.present();
}
 }

